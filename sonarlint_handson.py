import requests

def get():
    """DOcstring for get()
    """
    requests.get('https://www.google.com', verify=False)

def greater(num1, num2):
    """Greater method

    Args:
        num1 (int): [Variable 1]
        num2 (int): [variable 2]

    Returns:
        [int]: [answer]
    """
    if num1 > num2:
        return num1
    return num1

def add(num1: int, num2: int) -> int:
    """Add method

    Args:
        num1 (int): Number 1
        num2 (int): Number 2

    Returns:
        int: Result
    """
    return num1 + num2


def sample_method():
    """Docstring for function"""
def multiply(arg1, arg2, arg3,
    arg4):
    """The function multiplies five numbers"""
    return arg1 * arg2 * arg3 * arg4


def method(list1):
    """
    Docstring for given function
    """
    for i in list1:
        print(i)

def cylinder_volume(height, radius):
    """Volume of a cylinder
    Parameters:
    height (float): Height of the cylinder
    radius (float): Radius of the cylinder
    Returns:
    float:Volume of the cylinder
    """
    return height * 3.14159 * radius**2

def count_vowels(input_string):
    '''
    docstring for count_vowels
    '''
    words = input_string.split()
    word_vowel = dict()

    for word in words:
        count = 0
        for i in input_string:
            if i == 'a':
                count += 1
            if i == 'e':
                count += 1
            if i == 'i':
                count += 1
            if i == 'o':
                count += 1
            if i == 'u':
                count += 1
        word_vowel[word] = count
    return word_vowel



if __name__ == '__main__':

    # calling the function using keyword arguments
    cylinder_volume(height = 10.3, radius = 7.2)
    add('12', 23)
