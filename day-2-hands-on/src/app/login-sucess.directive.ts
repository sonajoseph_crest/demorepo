import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appLoginSucess]'
})
export class LoginSucessDirective {

  constructor(private ele:ElementRef) { 
    this.ele.nativeElement.style.color = "green"
  }

}
