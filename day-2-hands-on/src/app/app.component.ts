import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'day-2-hands-on';
  text = 'text';
  number = 'number';
  url='url';
  imageUrl = "";
  age = 0;
  username = "";
  container = "container"
  placeAge = "Enter your age"
  placeName = "Enter your name"
  placeImageUrl = "Enter Image Url"
  isEligible = false;
  checkEligible(){
    if(this.age>18)
    {
      this.isEligible = true
      return this.isEligible
    }
    else
    {
      this.isEligible =false
      return this.isEligible
    }
  }
  isValid = false
  
  save(){
    if(this.username == '' || this.age == 0 || this.url == '')
    {
      alert("Please fill details")
      this.isValid = false
    }
    else
    {
      this.isValid = true
    }
  }
}
