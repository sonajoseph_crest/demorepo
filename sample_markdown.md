# Main Heading

## H2 Heading

* **Bold Text**

* _Italic Text_

* Code Block
```
print('Hello World')
```

* Inline `code`

* Indented sub-points
    1. This is first point.
    2. This is another point.

## Table
| Browser | Version |
| ------- | ------- |
| Chrome | v96.0.4664.110 |
| Firefox | v91.0 |


Link to [Google](https://www.google.com)